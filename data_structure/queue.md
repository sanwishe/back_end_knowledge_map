#队列与栈

## Deque接口

从java1.6开始提供Deque接口，他表示一个可以从两端访问或者修改元素的线形集合。因此Dque可以作为队列和栈使用。

## ArrayDeque

ArrayDeque是Deque接口的可调整大小的实现。 ArrayDeque没有容量限制，可以根据需要增长容量以支持使用。 
 ArrayDeque不支持null元素。可以当作`stack`或者`queue`使用， 当用作`stack`时，ArrayDeque可能比Stack快，在用作`queue`时比LinkedList快。
 ArrayDeque是线程不安全的，在没有外部同步的情况下，不支持多线程的并发访问。和Stack比较，Stack是线程安全的，但是效率不高，因为它是`synchronized`的。在不需要线程安全的场所，ArrayDeque是可以完全替代Stack的。
和LinkedList、ArrayList一样，ArrayDeque的迭代器是`fast-fail`的。此外如果对ArrayDeque实例的修改发生在其迭代器创建之后，除了迭代器自身的`remove()`，所有的其它迭代器操作都会抛出` ConcurrentModificationException`异常。

这里ArrayDeque使用上和ArrayList以及LinkedList很相似，下面一个简单的示例：

```
package com.company;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.List;

public class CollectionTest {
    public static void main(String[] args) {
        List<String> list = new LinkedList<String>();

        list.add("12");
        list.add("34");
        list.add("56");
        list.add("78");

        ArrayDeque<String> deque = new ArrayDeque<String>();

        deque.add("90");
        deque.addAll(list);

        System.out.println(deque.remove("90"));

        deque.push("er");
        System.out.println(deque.pop());

        System.out.println(deque.peek());
        
        System.out.println(deque.poll());

        System.out.println(deque.pollLast());

        System.out.println(deque);
    }
}

```

输出：
![这里写图片描述](http://img.blog.csdn.net/20171220232729900?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvamlhbmdtaW5nemhpMjM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

可以看出：
- ArrayDeque可以作为`queue`使用，先`addFirst`然后`removeLast`
- ArrayDeque也可以作为`stack`使用，先`push`然后`pop`。

这里不在赘述。