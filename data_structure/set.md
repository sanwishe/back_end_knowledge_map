# 集合类

Set是一系列不重复元素的集合，而且最多包含一个null元素。
java中常见的Set接口的实现包括：HashSet，LinkedHashSet和TreeSet。

## HashSet

HashSet实现了Set接口，由一个哈希表（实际上是一个HashMap实例）缓存数据。对Set的迭代没有保证元素顺序，特别是不能保证订单会随着时间的推移而保持不变。HashSet允许null元素。 遍历HasHSet需要的时间与HashSet实例的大小（元素数量）加上支持HashMap实例的“容量”（桶的数量）的总和成正比。因此，如果迭代性能很重要，不要将初始容量设置得太高（或者负载因子太低）。由于HashMap是线程不安全的，所有HasHSet也是线程不安全的，这里不在赘述。
HashSet内部使用了HashMap：

```java
public HashSet() {
    map = new HashMap<>();
}
```

HashSet所有元素都存储在HashMap的KeySet中，其所有的value都是同一个常量：

```java
private static final Object PRESENT = new Object();
```

和HashMap一样，HashSet提供了四个个构造函数：

```java
public HashSet() {
    map = new HashMap<>();
}


public HashSet(Collection<? extends E> c) {
    map = new HashMap<>(Math.max((int) (c.size()/.75f) + 1, 16));
    addAll(c);
}

public HashSet(int initialCapacity, float loadFactor) {
    map = new HashMap<>(initialCapacity, loadFactor);
}
```

HashSet的默认容量为16，默认负载因子为0.75。

第四个构造函数这里不做详细叙述，它增加了第三个参数`boolean dummy`（但这个参数没有使用），但其初始化了一个LinkedHashMap。这个构造函数用于其子类LinkedHashSet实例化时调用。
HashSet只提供了基本的`add()`，`remove()`，`isEmpty()`，`contains()`，`size()`等几个简单的方法和一个迭代器用来放访问数据，当然也可以通过for循环访问HashSet的数据。

## LinkedHashSet

LinkedHashSet是基于LinkedList实现的HashSet。
它与HashSet的区别在于LinkedHashSet内部维护了一个双向链表，这个链表用来记录HashSet的迭代顺序。通常的迭代顺序就是元素的插入顺序，但如果一个元素重复插入，则其迭代顺序不会改变。如果需要接受输入并返回其有顺序的拷贝，则可以考虑使用LinkedHashSet。LinkedHashSet提供了所有可选的Set接口的操作，并允许null元素。
像HashSet一样，LinkedHashSet为基本操作（add()，contains()和delete()等）提供了恒定时间的性能。
由于增加了维护迭代顺序链表的开销，LinkedHashSet的性能可能略低于HashSet的性能。LinkedHashSet有两个影响其性能的参数：初始容量和负载因子。它们的定义与HashSet完全相同。但是请注意，对于这个类别，初始容量选择过高值的影响没有其对HashSet严重，因为这个类别的迭代次数不受容量的影响。 
最后，和HashSet一样，LinkedHashSet不是线程安全的。

LinkedHashSet构造函数调用了父类HashSet的具有三个参数的构造函数，内部实例化一个LinkedHashMap用来记录元素以及他们的迭代顺序。这里不在详细叙述，请参考LinkedHashMap。

## TreeSet

TreeSet 是一个有序的集合，它的作用是提供有序的Set集合。它继承于AbstractSet抽象类，实现了NavigableSet<E>, Cloneable, java.io.Serializable接口。因为TreeSet继承了AbstractSet抽象类，所以它是一个set集合，可以被实例化，且具有set的属性和方法。TreeSet是基于TreeMap实现的。TreeSet中的元素支持2种排序方式：自然排序 或者 根据创建TreeSet 时提供的 Comparator 进行排序。这取决于使用的构造方法。下面是支持 TreeSet 类的构造函数。

| 序号 | 构造函数的说明 |
| ----- |:------------------------:|
| 1 | TreeSet()  此构造函数构造空树集，将在根据其元素的自然顺序按升序排序。|
| 2 | TreeSet(c) 此构造函数生成树的集合，它包含的元素的集合 c。|
| 3 | TreeSet(comp) 此构造函数构造一个空树集，将根据给定的比较器进行排序。|
| 4 | TreeSet(SortedSet ss) 此构造函数生成包含给定SortedSet的元素的TreeSet|

TreeSet的性能比HashSet差但是我们在需要排序的时候可以用TreeSet因为他是自然排序也就是升序下面是TreeSet实现代码这个类也似只能通过迭代器迭代元素。
如果试图把一个对象添加到TreeSet时，则该对象的类必须实现Comparable接口，否则程序会抛出异常java.lang.ClassCastException。