# java map详解

java中的map有：HashMap，HashTable，LinkedHashMap，TreeMap，ConcurrentHashMap和WeakHashMap等。

## HashTable

HashTable是java中过时的默认hash表实现。HashTable的默认初始容量为11，而且HastTable不容许null的key和value。下面看一下HashMap的put方法：

```java
public synchronized V put(K key, V value) {
    // Make sure the value is not null
    if (value == null) {
        throw new NullPointerException();
    }
    // Makes sure the key is not already in the hashtable.
    Entry<?,?> tab[] = table;
    int hash = key.hashCode();
    int index = (hash & 0x7FFFFFFF) % tab.length;
    @SuppressWarnings("unchecked")
    Entry<K,V> entry = (Entry<K,V>)tab[index];
    for(; entry != null ; entry = entry.next) {
        if ((entry.hash == hash) && entry.key.equals(key)) {
            V old = entry.value;
            entry.value = value;
            return old;
        }
    }
    addEntry(hash, key, value, index);
    return null;
}
```

HashTable的put方法是同步方法，也就是说HashTable是线程安全的，但是由于同步方法，也可以推导出HashTable的性能比较低。

## HashMap

HashMap是基于哈希表的Map实现，它提供了所有可选的映射操作，并允许null的value和null的key。(HashMap类似于Hashtable, 除了HashMap是线程不安全的，并允许null)。HashMap不保证映射顺序，而且也不保证顺序在一段时间内保持不变。
HashMap的迭代需要与其实例的容量(Hash桶数)与其size(键值映射数)成比例。因此，如果迭代性能很重要，就不要将初始容量设置得太高(或负载因子太低)。HashMap的一个实例有两个影响其性能的参数：初始容量和负载因子。容量是Hash表中的Hash桶数，初始容量只是创建哈希表时的容量；负载因子是在容量自动增加之前允许哈希表得到满足的度量。当哈希表中的条目数超过负载因子和当前容量的乘积时，就重新排列哈希表(这时HashMap内部数据结构被重新构建，以使散列表具有大约两倍的桶数，即容量增加为原来的2倍。默认负载因子(0.75)提供了时间和空间成本之间的良好折中，初始容量为16，如果初始容量大于最大条目数除以负载因子, 则不会发生rehash操作。需要注意，HashMap是线程不安全的，如果多个线程并发访问同一个HashMap实例, 并且至少有一个线程在结构上修改了映射, 那么它必须在外部进行同步。可以使用 `Collections.synchronizedMap`方法“包装”Map以获取线程安全的HashMap，但是不推荐这么做。HashMap的迭代器都是`fast-fail`的，即如果Map在迭代器创建之后的任何时间被结构化地修改, 除了通过迭代器自己的remove方法之外, 都会马上抛出一个ConcurrentModificationException异常。

总结一下：

 1. HashMap容许空Key和空value
 2. HashMap默认有两个初始参数：initial capacity和load factor
 3. HashMap不是线程安全的

### HashMap的数据存储

HashMap的数据存储在`Node<K, V> [] table`的数组中。先看看Node的定义。

```java
static class Node<K,V> implements Map.Entry<K,V> {
    final int hash;
    final K key;
    V value;
    Node<K,V> next;

    Node(int hash, K key, V value, Node<K,V> next) {
        this.hash = hash;
        this.key = key;
        this.value = value;
        this.next = next;
    }
    //......
}
```

存储数组的每一个节点都是一个单链表，链表的每一个节点存储该节点数据的key，value和hashCode。再看看插入操作：

```java
final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                   boolean evict) {
        Node<K,V>[] tab; Node<K,V> p; int n, i;
        if ((tab = table) == null || (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((p = tab[i = (n - 1) & hash]) == null)
            tab[i] = newNode(hash, key, value, null);
        else {
            Node<K,V> e; K k;
            if (p.hash == hash &&
                ((k = p.key) == key || (key != null && key.equals(k))))
                e = p;
            else if (p instanceof TreeNode)
                e = ((TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
            else {
                for (int binCount = 0; ; ++binCount) {
                    if ((e = p.next) == null) {
                        p.next = newNode(hash, key, value, null);
                        if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                            treeifyBin(tab, hash);
                        break;
                    }
                    if (e.hash == hash &&
                        ((k = e.key) == key || (key != null && key.equals(k))))
                        break;
                    p = e;
                }
            }
            if (e != null) { // existing mapping for key
                V oldValue = e.value;
                if (!onlyIfAbsent || oldValue == null)
                    e.value = value;
                afterNodeAccess(e);
                return oldValue;
            }
        }
        ++modCount;
        if (++size > threshold)
            resize();
        afterNodeInsertion(evict);
        return null;
    }
```

总结一下，插入数据的操作，步骤如下：

1. 如果table数组为空，调用`resize()`为table分配空间，得到table大小为`n`。
2. 如果key的`hashCode`和`n`计算key的索引i，`i=(n - 1) & hash`，如果table数组中索引为`i`的元素为空，则直接new一个Node对象存储到该位置。
3. 否则，索引为`i`的位置不为null，则从该节点的链表的头部依次迭代，如果发现链表的这个节点`p`和要插入数据`p`的key满足：`p.hash == hash && ((k = p.key) == key || (key != null && key.equals(k)))`，则替换原来的节点p。
4. 如果遍历单链表也没有找到满足`p.hash == hash && ((k = p.key) == key || (key != null && key.equals(k)))`的节点，则将插入的值放到该链表的尾部。
5. 如果该节点的单链表长度大于8，则需要将单链表转化为红黑树，即其中的`treeifyBin`操作。
6. 修改内部属性`modCount`和`size`的值，如果修改后的`size`大于`threshold`，则需要执行`resize（）`操作。

对于get操作，核心代码如下：

```java
final Node<K,V> getNode(int hash, Object key) {
        Node<K,V>[] tab; Node<K,V> first, e; int n; K k;
        if ((tab = table) != null && (n = tab.length) > 0 &&
            (first = tab[(n - 1) & hash]) != null) {
            if (first.hash == hash && // always check first node
                ((k = first.key) == key || (key != null && key.equals(k))))
                return first;
            if ((e = first.next) != null) {
                if (first instanceof TreeNode)
                    return ((TreeNode<K,V>)first).getTreeNode(hash, key);
                do {
                    if (e.hash == hash &&
                        ((k = e.key) == key || (key != null && key.equals(k))))
                        return e;
                } while ((e = e.next) != null);
            }
        }
        return null;
    }
```

大概思路如下：

1. 通过给定的key计算出hashCode，根据`(n - 1) & hash`计算出桶的位置。
2. 找到桶，如果桶不为空，判断桶中链表的第一个节点`first`是否满足`first.hash == hash && ((k = first.key) == key || (key != null && key.equals(k)))`，如果满足，返回该节点的value值。
3. 遍历这个单链表，如果有节点满足上述条件，返回该节点的value。
4. 如果`index`桶中的元素为`TreeNode`对象，则这个桶中的KV对数量大于8，该桶为一颗红黑树，调用红黑树的`getTreeNode(key, hash)`方法查找key对于的节点，并返回value。
5. 其他情况，返回null。

### resize过程

```java
final Node<K,V>[] resize()
```

resize用来初始化或者将table的容量翻倍。如果table为null， 则为其分配初始容量（16）。步骤如下：

1. 根据当前的容量(`oldTab.length`)和门限`threshold`，计算出resize之后的容量并更新会内部属性，在不超出最大容量（`1>>30`）前提下，容量翻倍。
2. 遍历table，如果某个位置`j`的桶不为空，并且桶中的链表只有一个节点`e`，直接将其放到新的newTab中，`newTab[e.hash & (newCap - 1)] = e`
3. 如果这个桶中的节点`e`是一个`TreeNode`的节点，需要将这个红黑树拆两棵树，拆分的依据是：`(e.hash & oldCap) == 0`，为0时放到`loHead`中，不为0时放到`hiHead`中。然后将`loHead`放到新表newTab的`j`索引的位置。`HiHead`放到newTab的的`j+oldCao`位置。
4. 如果桶中的元素为链表的头结点，按照同样的方式拆分到新的两个新的链表`loHead`和`hiHead`位置。然后将`loHead`放到新表newTab的`j`索引的位置。`HiHead`放到newTab的的`j+oldCao`位置。
5. 遍历所有桶，重复2、3、4步直到结束。

### HashMap和其它Map对比

(1) HashMap：它根据键的hashCode值存储数据，大多数情况下可以直接定位到它的值，因而具有很快的访问速度，但遍历顺序却是不确定的。 HashMap最多只允许一条记录的键为null，允许多条记录的值为null。HashMap非线程安全，即任一时刻可以有多个线程同时写HashMap，可能会导致数据的不一致。如果需要满足线程安全，可以用 Collections的synchronizedMap方法使HashMap具有线程安全的能力，或者使用ConcurrentHashMap。

(2) Hashtable：Hashtable是遗留类，很多映射的常用功能与HashMap类似，不同的是它承自Dictionary类，并且是线程安全的，任一时间只有一个线程能写Hashtable，并发性不如ConcurrentHashMap，因为ConcurrentHashMap引入了分段锁。Hashtable不建议在新代码中使用，不需要线程安全的场合可以用HashMap替换，需要线程安全的场合可以用ConcurrentHashMap替换。

(3) LinkedHashMap：LinkedHashMap是HashMap的一个子类，保存了记录的插入顺序，在用Iterator遍历LinkedHashMap时，先得到的记录肯定是先插入的，也可以在构造时带参数，按照访问次序排序。

(4) TreeMap：TreeMap实现SortedMap接口，能够把它保存的记录根据键排序，默认是按键值的升序排序，也可以指定排序的比较器，当用Iterator遍历TreeMap时，得到的记录是排过序的。如果使用排序的映射，建议使用TreeMap。在使用TreeMap时，key必须实现Comparable接口或者在构造TreeMap传入自定义的Comparator，否则会在运行时抛出java.lang.ClassCastException类型的异常。

对于上述四种Map类型的类，要求映射中的key是不可变对象。不可变对象是该对象在创建后它的哈希值不会被改变。如果对象的哈希值发生变化，Map对象很可能就定位不到映射的位置了。

### 如何怎么HashMap的线程不安全性

#### 并发的put操作

代码如下

```java
package com.jmz;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class Main {

    private final static Map<String, String> map = new HashMap<>();


    public static void main(String[] args) throws InterruptedException {
	    // write your code here
        CountDownLatch latch = new CountDownLatch(4);

        for (int i = 0; i < 4; i++) {
            new Thread(() -> {
                final String name = Thread.currentThread().getName();
                for (int j = 0; j < 1000; j++) {
                    map.put(name + j, "p");
                }

                latch.countDown();
            }).start();
        }

        latch.await();

        System.out.println(map.size());
    }
}

```

这段代码运行时，可能会陷入死循环，或者结果不是4000，或者抛出异常
![这里写图片描述](https://img-blog.csdn.net/20180420225853315?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2ppYW5nbWluZ3poaTIz/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
或者时一个小于4000的整数
![这里写图片描述](https://img-blog.csdn.net/20180420225928873?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2ppYW5nbWluZ3poaTIz/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

作为对比，将上述代码第14行修改为

```java
private final static Map<String, String> map = Collections.synchronizedMap(new HashMap<>());
```

或者

```java
private final static Map<String, String> map = new ConcurrentHashMap<>();
```

时，结果始终为4000

#### 并发修改

在迭代器迭代过程中修改原集合

## LinkedHashMap

顾名思义，LinkedHashMap是基于链表实现的有序的HashMap。LinkedHashMap内部维护了一个双向链表，链表定义了其元素迭代的顺序，通常的顺序就是元素被插入到LinkedHashMap的顺序。LinkedHashMap摒弃了HashMap的乱序问题，同时也不像TreeMap一样有较高的开销。LinkedHashMap可以用于生成和原始Map顺序一样的拷贝，它提供了一个可以传入任意非空Map的构造函数。
LinkedHashMap提供两种迭代顺序：最后访问顺序和元素插入顺序（由LinkedHashMap内部属性`accessOrder`决定，为`true`时，迭代顺序为最后访问顺序，否则为元素插入顺序，默认为`fasle`）。
LinkedHashMap实例调用`put()`， `putIfAbsent()`， `get()`， `getOrDefault()`， `compute()`， `computeIfAbsent()`， `computeIfPresent()`或 `merge()`方法将导致访问相应的元素。  `putAll()`方法为指定Map中的每个KV对生成一次访问，访问顺序就是指定Map的原始的迭代器迭代顺序。除了上述方法，其他方法不会生成访问。
LinkedHashMap实现了Map接口的全部操作，允许null，由于需要维持链表顺序，其基本操作的性能相对HashMap稍差。并且如果定义了最后访问顺序排序的LinkedHashMap，其`get()`操作会对其结构作出修改。此外，LinkedHashMap是线程不安全的。

由于LinkedHashMap继承了HashMap，我们这里仅讲述一下不同的地方，相同的操作本文将略过。

### LinkedHashMpa的构造函数

LinkedHashMap的Entry继承自`HasHMap.Node<K,V>`，在Entry中增加了到前后节点的reference，用来记录LinkedHashMap实例的迭代顺序。并且记录了双向链表的head和tail，用来按顺序迭代其元素。

```java
static class Entry<K,V> extends HashMap.Node<K,V> {
    Entry<K,V> before, after;
    Entry(int hash, K key, V value, Node<K,V> next) {
        super(hash, key, value, next);
    }
}
```

除了HashMap指定的三个构造函数，LinkedHashMap提供了第四个构造函数，他可以用来指定LinkedHashMap的迭代顺序是按照访问顺序还是插入顺序。

```java
public LinkedHashMap(int initialCapacity,
                     float loadFactor,
                     boolean accessOrder) {
    super(initialCapacity, loadFactor);
    this.accessOrder = accessOrder;
}
```

不指定`accessOrder`时，**默认其值为`false`，即按照插入顺序迭代**。下面两节分别讲述一下插入元素和访问元素是的链表操作：

### LinkedHashMap的插入元素操作

LinkedHashMap覆盖了HashMap的`newNode()`方法，增加了将新节点link到双向链表的尾部的操作：

```java
private void linkNodeLast(LinkedHashMap.Entry<K,V> p) {
    LinkedHashMap.Entry<K,V> last = tail;
    tail = p;
    if (last == null)
        head = p;
    else {
        p.before = last;
        last.after = p;
    }
}
```

这个操作完成了元素插入顺序的记录。

同样，删除某个KV对时，LinkedHashMap覆盖的方法`afterNodeRemoval(node)`会在删除节点的`removeNode()`后调用，这个操作会生出迭代顺序链表中的相应节点。

```java
void afterNodeRemoval(Node<K,V> e) {
    LinkedHashMap.Entry<K,V> p =
        (LinkedHashMap.Entry<K,V>)e, b = p.before, a = p.after;
    p.before = p.after = null;
    if (b == null)
        head = a;
    else
        b.after = a;
    if (a == null)
        tail = b;
    else
        a.before = b;
}
```

此外插入一个映射后，如果这个Map被定义为创建模式（`evict`为`false`），而且要求删除年龄最大的映射时，需要将将链表的头部节点删除。不过当前的实现里，`evict`始终为`true`，而且`removeEldestEntry()`始终返回`fasle`，即jdk的LinkedHashMap和HashMap实现中，Map实例始终都不会是创建模式而去不删除年龄最老的映射。

```java
void afterNodeInsertion(boolean evict) {
    LinkedHashMap.Entry<K,V> first;
    if (evict && (first = head) != null && removeEldestEntry(first)) {
        K key = first.key;
        removeNode(hash(key), key, null, false, true);
    }
}
```

### LinkedHashMap的访问元素操作

访问某个节点后，会回调`afterNodeAccess()`方法，将访问的节点移动到链表的**尾部**，访问某个节点的操作包括：`put()`，`replace()`，`computeIfAbsent()`，`computeIfPresent()`，`compute()`，`merge()`，以及`get()`，`getOrDefault()`。

```java
void afterNodeAccess(Node<K,V> e) { // move node to last
    LinkedHashMap.Entry<K,V> last;
    if (accessOrder && (last = tail) != e) {
        LinkedHashMap.Entry<K,V> p = (LinkedHashMap.Entry<K,V>)e, b = p.before, a = p.after;
        p.after = null;
        if (b == null)
            head = a;
        else
            b.after = a;
        if (a != null)
            a.before = b;
        else
            last = b;
        if (last == null)
            head = p;
        else {
            p.before = last;
            last.after = p;
        }
        tail = p;
        ++modCount;
    }
}
```

## TreeMap

先看看TreeMap的继承关系：
![TreeMap继承关系](http://img.blog.csdn.net/20180101184324869?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvamlhbmdtaW5nemhpMjM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

TreeMap是一个基于“红黑树”实现的导航映射（`NavigableMap`）。其映射的Key根据自然顺序排列，或者根据映射创建时提供的比较器排列。
由于TreeMap继承了SortedMap，这要求TreeMap的key必须是可比较的：要么提供`compareTo()`方法（实现`Comparable`）接口，要么在映射初始化的时候提供比较器Comparator。

主意，TreeMap时线程不安全的，但可以使用包装方法得到线程安全的TreeMap实例：

```java
SortedMap m = Collections.synchronizedSortedMap(new TreeMap(...));
```

### 红黑树介绍

#### 二叉树

二叉树是树的特殊一种，具有如下特点：

- 每个结点最多有两颗子树
- 左子树和右子树是有顺序的，次序不能颠倒
- 即使某结点只有一个子树，也要区分左右子树

二叉树遍历：从树的根节点出发，按照某种次序依次访问二叉树中所有的结点，使得每个结点被访问仅且一次。这里有两个关键词：访问和次序。

二叉树常见的访问方式：

##### 二叉树的前序遍历

基本思想：先访问根结点，再先序遍历左子树，最后再先序遍历右子树即：**根->左->右**。
![这里写图片描述](http://img.blog.csdn.net/20180115222321461?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvamlhbmdtaW5nemhpMjM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
上图中的的元素访问顺序： 1  -> 2 -> 4 -> 5 -> 7 -> 8 -> 3 -> 6。

##### 二叉树的中序遍历

基本思想：先中序遍历左子树，然后再访问根结点，最后再中序遍历右子树即：**左 -> 根 -> 右**。
![这里写图片描述](http://img.blog.csdn.net/20180115222635519?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvamlhbmdtaW5nemhpMjM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

上图中的二叉树安中序遍历的访问顺序是： 4 -> 2 -> 7 -> 8 -> 5 -> 1 -> 3 -> 6。

##### 二叉树的后序遍历

基本思想：先后序遍历左子树，然后再后序遍历右子树，最后再访问根结点即：**左 -> 右 -> 根**。

![这里写图片描述](http://img.blog.csdn.net/20180115222909458?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvamlhbmdtaW5nemhpMjM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

上图中的二叉树按后序遍历的结果是：4 -> 8 -> 7 -> 5 -> 2 -> 6 -> 3 -> 1。

#### 红黑树的概念

R-B Tree，全称是Red-Black Tree，又称为“红黑树”，它一种特殊的二叉查找树。红黑树的每个节点上都有存储位表示节点的颜色，可以是红(Red)或黑(Black)。

红黑树的特性:

- 每个节点或者是黑色，或者是红色。
- 根节点是黑色。
- 每个叶子节点（NIL）是黑色，注意：这里叶子节点，是指为空(NIL或NULL)的叶子节点。
- 如果一个节点是红色的，则它的子节点必须是黑色的。
- 从一个节点到该节点子树包含的的叶子节点的所有路径上包含相同数目的黑节点。

如下图示：
 ![这里写图片描述](http://img.blog.csdn.net/20180115223756749?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvamlhbmdtaW5nemhpMjM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

红黑树的时间复杂度为O(lgn)，因此应用非常广泛，如本文中的TreeMap。

##### 红黑树的基本操作

红黑树的基本操作是添加、删除。在对红黑树进行添加或删除之后，都会用到旋转方法。为什么呢？道理很简单，添加或删除红黑树中的节点之后，红黑树就发生了变化，可能不满足红黑树的5条性质，也就不再是一颗红黑树了，而是一颗普通的树。而通过旋转，可以使这颗树重新成为红黑树。简单点说，旋转的目的是让树保持红黑树的特性。
旋转包括两种：左旋 和 右旋。

###### 左旋与右旋

当在某个节点x进行左旋时，假设它的右孩子节点y不是T.nil，x可以为其右孩子不是T.nil节点的树内任意节点，左旋x到y的链，使得y成为该子树新的根节点，x成为y的左孩子，y的左孩子成为x的右孩子，x的左孩子仍旧为x的左孩子。

![这里写图片描述](http://img.blog.csdn.net/20180116234735179?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvamlhbmdtaW5nemhpMjM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

左旋的伪代码：

```c++
y = x.right         // set y
x.right = y.left    // 将y的左孩子设置为x的右孩子
if y.left != T.nil
    y.left.parent = x
y.parent = x.parent
if x.parent == T.nil
    T.root = y
else if x == x.parent.left
    x.parent.left = y
else 
    x.parent.right = y
    y.left = x
    x.parent = y

```

对x进行左旋，意味着，将“x的右孩子”设为“x的父亲节点”；即，将 x变成了一个左节点。 因此，左旋中的“左”，意味着“被旋转的节点将变成一个左节点”。
对x进行右旋，意味着，将“x的左孩子”设为“x的父亲节点”；即，将 x变成了一个右节点。因此，右旋中的“右”，意味着“被旋转的节点将变成一个右节点”。

###### 插入

将一个节点插入到红黑树中，需要执行哪些步骤呢？首先，将红黑树当作一颗二叉查找树，将节点插入；然后，将节点着色为红色；最后，通过旋转和重新着色等方法来修正该树，使之重新成为一颗红黑树。详细描述如下：

第一步: 将红黑树当作一颗二叉查找树，将节点插入。
       红黑树本身就是一颗二叉查找树，将节点插入后，该树仍然是一颗二叉查找树。也就意味着，树的键值仍然是有序的。此外，无论是左旋还是右旋，若旋转之前这棵树是二叉查找树，旋转之后它一定还是二叉查找树。这也就意味着，任何的旋转和重新着色操作，都不会改变它仍然是一颗二叉查找树的事实。

第二步：将插入的节点着色为"红色"。
       将插入的节点着色为红色，不会违背"特性(5)"。少违背一条特性，就意味着我们需要处理的情况越少。

第三步: 通过一系列的旋转或着色等操作，使之重新成为一颗红黑树。
       第二步中，将插入节点着色为"红色"之后，不会违背"特性(5)"。那它到底会违背哪些特性呢？
       对于"特性(1)"，显然不会违背了。因为我们已经将它涂成红色了。
       对于"特性(2)"，显然也不会违背。在第一步中，我们是将红黑树当作二叉查找树，然后执行的插入操作。而根据二叉查找数的特点，插入操作不会改变根节点。所以，根节点仍然是黑色。
       对于"特性(3)"，显然不会违背了。这里的叶子节点是指的空叶子节点，插入非空节点并不会对它们造成影响。
       对于"特性(4)"，是有可能违背的！
       那接下来，想办法使之"满足特性(4)"，就可以将树重新构造成红黑树了。

### TreeMap实现介绍

```java
static final class Entry<K,V> implements Map.Entry<K,V> {
        K key;
        V value;
        Entry<K,V> left;
        Entry<K,V> right;
        Entry<K,V> parent;
        boolean color = BLACK;

        /**
         * Make a new cell with given key, value, and parent, and with
         * {@code null} child links, and BLACK color.
         */
        Entry(K key, V value, Entry<K,V> parent) {
            this.key = key;
            this.value = value;
            this.parent = parent;
        }

        /**
         * Returns the key.
         *
         * @return the key
         */
        public K getKey() {
            return key;
        }

        /**
         * Returns the value associated with the key.
         *
         * @return the value associated with the key
         */
        public V getValue() {
            return value;
        }

        /**
         * Replaces the value currently associated with the key with the given
         * value.
         *
         * @return the value associated with the key before this method was
         *         called
         */
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        public boolean equals(Object o) {
            if (!(o instanceof Map.Entry))
                return false;
            Map.Entry<?,?> e = (Map.Entry<?,?>)o;

            return valEquals(key,e.getKey()) && valEquals(value,e.getValue());
        }

        public int hashCode() {
            int keyHash = (key==null ? 0 : key.hashCode());
            int valueHash = (value==null ? 0 : value.hashCode());
            return keyHash ^ valueHash;
        }

        public String toString() {
            return key + "=" + value;
        }
    }
```

TreeMap中红黑树的左旋与右旋

```java
    /** From CLR */
    private void rotateLeft(Entry<K,V> p) {
        if (p != null) {
            Entry<K,V> r = p.right;
            p.right = r.left;
            if (r.left != null)
                r.left.parent = p;
            r.parent = p.parent;
            if (p.parent == null)
                root = r;
            else if (p.parent.left == p)
                p.parent.left = r;
            else
                p.parent.right = r;
            r.left = p;
            p.parent = r;
        }
    }

    /** From CLR */
    private void rotateRight(Entry<K,V> p) {
        if (p != null) {
            Entry<K,V> l = p.left;
            p.left = l.right;
            if (l.right != null) l.right.parent = p;
            l.parent = p.parent;
            if (p.parent == null)
                root = l;
            else if (p.parent.right == p)
                p.parent.right = l;
            else p.parent.left = l;
            l.right = p;
            p.parent = l;
        }
    }

```

### 应用示例

示例1：

```java
public class ScatterChartSerializer extends JsonSerializer<ScatterChart> {

    @Override
    public void serialize(ScatterChart scatterChart, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        //construct Treemap instance with an comparator
        Map<String, Integer> rangeMap = new TreeMap<>(Comparator.comparingInt(OrderedElapsedRanger::getOrder).reversed());

        List<ScatterData> scatterDataList = scatterChart.getScatterData();
        if (null != scatterDataList && !scatterDataList.isEmpty()) {
            jsonGenerator.writeFieldName("data");
            jsonGenerator.writeStartArray();
            for (ScatterData scatterData : scatterDataList) {
                Map<String, ScatterAggregation> scatterMap = scatterData.getRangeMap();
                if (null == scatterMap || scatterMap.isEmpty()) {
                    continue;
                }

                for (Map.Entry<String, ScatterAggregation> entry : scatterMap.entrySet()) {
                    jsonGenerator.writeStartArray();
					//do something serializing metadata
                    Integer count = rangeMap.get(entry.getKey());

                    // insert kv map and sort them
                    if (count == null) {
                        rangeMap.put(entry.getKey(), entry.getValue().getCount());
                    } else {
                        rangeMap.put(entry.getKey(), count + entry.getValue().getCount());
                    }


                    jsonGenerator.writeEndArray();
                }
            }
            jsonGenerator.writeEndArray();

            jsonGenerator.writeFieldName("range");

            jsonGenerator.writeStartArray();
            //encounter sorted kv map
            for (Map.Entry<String, Integer> entry : rangeMap.entrySet()) {
                //do something serializing elapsed range
            }
            jsonGenerator.writeEndArray();
        }

        jsonGenerator.writeEndObject();
    }
}
```

示例2：

```java
public class TreeMapTest {

    public static void main(String[] args) {
        Map<Integer, String> map = new TreeMap<>();

        map.put(1, "2");
        map.put(32, "32");
        map.put(22, "22");
        map.put(8, "8");
        map.put(12, "12");

        for (Map.Entry<Integer, String> integerStringEntry : map.entrySet()) {
            System.out.println("key   = " + integerStringEntry.getKey());
            System.out.println("value = " + integerStringEntry.getValue());
        }
    }
}

```

输出：

```bash
"D:\Program Files\Java\bin\java" ......
key   = 1
value = 2
key   = 8
value = 8
key   = 12
value = 12
key   = 22
value = 22
key   = 32
value = 32

Process finished with exit code 0
```