# ArrayList与LinkedList

## ArrayList

ArrayList是我们java编码过程中最常用的集合类型。它在java collection framework中的位置如下：
![这里写图片描述](http://img.blog.csdn.net/20171217003041034?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvamlhbmdtaW5nemhpMjM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

ArrayList是List的可变大小实现，它实现了List的所有可选操作，允许所有元素，包括null。其内部使用一维数组存储数据，因此可以通过索引随机访问。`size()`， `isEmpty()`， `get()`，  `set()`， `iterator()` 和`listIterator()`操作的复杂度都是`O(1)`。
ArrayList可以在插入数据前，通过`ensureCapacity`一次性分配足够的空间，而不会因为随着输入逐个插入而不断进行增加空间的操作而浪费性能。
ArrayList是线程不安全的，如果需要线程安全的List，如果需要线程安全的实现，需要使用`Collections.synchronizedList`方法包装，这个方法最好在ArrayList创建的时候就执行：

```java
List list = Collections.synchronizedList(new ArrayList(...));
```

在并发修改ArrayList时，迭代器会快速而直接地抛出`ConcurrentModificationException`异常，而不是在后续不确定的时间带来不确定的风险。

## LinkedList

LinkedList是List接口的双向链表实现。它继承了`AbstractSequentialList`抽象类，这个抽象类我们在[AbstractList](http://blog.csdn.net/jiangmingzhi23/article/details/78821726)中讨论过，AbstractSequentialList用于抽象“循序访问”的集合。LinkedList还实现了List接口和Deque接口，List接口大家都很熟悉，Deque接口用来表示一个两端可操作的线性集合，是“*Double ended Queue*”的缩写。
基于Deque接口的特性，LinkedList需要知道集合的头部和尾部，以实现两端插入或者删除数据的操作。
同时，LinkedList作为一个List，因此也需要提供List接口的`add(int, E)`，`set(int, E)`、`get(int)`，`remove(int)`等“随机访问”的操作。当然，作为`AbstractSequentialList`的实现类，LinkedList没有实现`RandomAccess`，说明它不支持类似数组的按index随机访问的具体操作。上述LinkedList实现的List接口的一些操作，都是通过链表顺序访问的一种封装，最终所谓的“随机访问”还是需要按照链表的顺序访问方式类实现。

需要注意的是，LinkedList不是线程安全的。如果需要多线程访问LinkedList，可以通过`Collections.synchronizedList`方法包装：

```java
List list = Collections.synchronizedList(new LinkedList(...));
```

同样地，作为List，LinkedList的迭代器也是*fail-first*的，即迭代器创建后，任何结构性的修改都会抛出`ConcurrentModificationException`异常，但用迭代器自己的remove和add方法除外。

## ArrayList内部属性

ArrayList继承自AbstractList，实现了RandomAccess接口、List接口、Serializable接口和Cloneable接口，这表明：

 - ArrayList是一个可以“随机访问”的List
 - ArrayList可以被序列化到流
 - ArrayList可以调用clone（）方法进行浅拷贝

它定义了以下几个常量：

```java
//ArrayList的默认容量是10,相比较，HashMap初始容量为16,LinkedList只内部只保留了链表首尾，没有定义默认容量
private static final int DEFAULT_CAPACITY = 10;

//用于空实例的共享数组,用在initialCapacity为0时的ArrayList的初始化
private static final Object[] EMPTY_ELEMENTDATA = {};

//用于默认大小的空实例的共享数据数组，和EMPTY_ELEMENTDATA区分开，以知道第一个元素加进来之后，ArrayList扩展了多少，用于无参ArrayList（）初始化ArrayList
private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};

//用来存储ArrayList的数据，默认容量初始化时，即为DEFAULTCAPACITY_EMPTY_ELEMENTDATA；非私有以方便内部类范围数据。
//ArrayList实际上是一个数组，所以通过数组下标（索引）直接访问数据。
transient Object[] elementData; 

//ArrayList实例的大小
private int size;

//ArrayList的最大容量，由于某些JVM实现中，array有一些头部信息，所以最大值不是Integer.MAX_VALUE - 1
private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

```

## ArrayList的构造函数

### 无参构造函数，默认容量为10.

```java
public ArrayList() {
    this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
}
```

### 带初始容量的构成函数

```java
public ArrayList(int initialCapacity) {
    if (initialCapacity > 0) {
        this.elementData = new Object[initialCapacity];
    } else if (initialCapacity == 0) {
        this.elementData = EMPTY_ELEMENTDATA;
    } else {
        throw new IllegalArgumentException("IllegalCapacity: " + initialCapacity);
    }
}
```

### 通过其他指定的集合作为构造函数参数，将提供的集合转成数组返回给elementData

```java
public ArrayList(Collection<? extends E> c) {
    elementData = c.toArray();
    if ((size = elementData.length) != 0) {
        // c.toArray might (incorrectly) not return Object[] (see 6260652)
        if (elementData.getClass() != Object[].class)
            elementData = Arrays.copyOf(elementData, size, Object[].class);
    } else {
        // replace with empty array.
        this.elementData = EMPTY_ELEMENTDATA;
    }
}
```

## LinkedList的构造函数

LinkedList拥有无参构造函数，用于创建一个空的链表，即size为0，first和last都为空的链表。另一个通过现有集合创建链表，参数为指定的非空集合，然后调用`addAll(int, Collection<E>)`插入所有元素。如果指定集合为`null`，则抛出空指针错误。

## LinkedList 内部数据存储

LinkedList采用链表存储数据，每一个节点存储了当前元素引用，前一个元素的引用和后一个元素的引用。节点数据结构如下：

```java
private static class Node<E> {
    E item;
    Node<E> next;
    Node<E> prev;

    Node(Node<E> prev, E element, Node<E> next) {
        this.item = element;
        this.next = next;
        this.prev = prev;
    }
}
```

LinkedList内部记录着链表的头部和尾部，以及链表节点数目。

```java
transient int size = 0;

transient Node<E> first;

transient Node<E> last;
```

所有的中间节点的访问都需要通过first和last顺序访问。因此访问第index个节点的复杂度为`O(index + 1)`或者`O(size-index + 1)`。

相比较，ArrayList内部使用数组存储数据

## ArrayList方法总结

### 容量相关的方法：

```java
private void ensureCapacityInternal(int minCapacity) {
    if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
        minCapacity = Math.max(DEFAULT_CAPACITY, minCapacity);
    }

    ensureExplicitCapacity(minCapacity);
}

private void ensureExplicitCapacity(int minCapacity) {
    modCount++;

    // overflow-conscious code
    if (minCapacity - elementData.length > 0)
        grow(minCapacity);
}

private void grow(int minCapacity) {
    // overflow-conscious code
    int oldCapacity = elementData.length;
    int newCapacity = oldCapacity + (oldCapacity >> 1);
    if (newCapacity - minCapacity < 0)
        newCapacity = minCapacity;
    if (newCapacity - MAX_ARRAY_SIZE > 0)
        newCapacity = hugeCapacity(minCapacity);
    // minCapacity is usually close to size, so this is a win:
    elementData = Arrays.copyOf(elementData, newCapacity);
}

private static int hugeCapacity(int minCapacity) {
    if (minCapacity < 0) // overflow
        throw new OutOfMemoryError();
    return (minCapacity > MAX_ARRAY_SIZE) ? Integer.MAX_VALUE : MAX_ARRAY_SIZE;
}

```

总结，在`ensureCapacity`操作中，ArrayList实例原来的容量为oldCapacity，操作后容量为newCapacity，则`newCapacity = oldCapacity + (oldCapacity >> 1);`，当然，newCapacity要在`MAX_ARRAY_SIZE`以内。
ArrayList的实现中，所有修改操作前，都要进行`ensureCapacityInternal`以保证容量。并对外提供`void ensureCapacity(int minCapacity)`的public方法供开发者调用。

索引检查
所有的和具体的索引修改的操作，都需要坚持index在ArrayList的存储数组范围内。index范围检查的方法包括`rangeCheck（int）`和`rangeCheckForAdd（int）`，如果索引超出范围，会抛出`IndexOutOfBoundsException`异常。

### add方法

```java
public void add(int index, E element) {
        rangeCheckForAdd(index);

        ensureCapacityInternal(size + 1);  // Increments modCount!!
        System.arraycopy(elementData, index, elementData, index + 1,
                         size - index);
        elementData[index] = element;
        size++;
    }
```

先检查index是否合法，然后给ArrayList扩容，调用`System.arraycopy`将索引为`index`之后的子数组后移一个单位，并将数组中索引为`index`元素的值设置为`element`。`System.arraycopy`是一个本地方法，描述如下：
![这里写图片描述](http://img.blog.csdn.net/20171217165303944?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvamlhbmdtaW5nemhpMjM=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)
其作用就是放数组`src`的第`srcPos`个及之后`length`个元素拷贝到`dest`数组的第`destPos`之后。
上面的代码显示，ArrayList增加元素的操作分为几个步骤：

- 增加缓存数组的大小：`ensureCapacityInternal`
- 将`index`位置及以后的元素后移一个索引
- 将`index`位置设置为指定的`element`
- `size++`

一个add操作分为四个步骤，所以不满足线程安全的条件：原子性。这也可以说是ArrayList线程不安全的理由。

### 基于索引的remove方法

```java
public E remove(int index) {
    rangeCheck(index);

    modCount++;
    E oldValue = elementData(index);

    int numMoved = size - index - 1;
    if (numMoved > 0)
        System.arraycopy(elementData, index+1, elementData, index, numMoved);
    elementData[--size] = null; // clear to let GC do its work

    return oldValue;
}
```

ArrayList的实现直接将`index`之后的元素前移一个单位，覆盖原来第`index`个元素，将`size`减一，原尾部元素设为null，返回被删除的元素应用。删除元素导致数组长度变化，因此不能通过索引在for循环中删除元素。

### 基于元素的remove方法

```java
public boolean remove(Object o) {
   if (o == null) {
        for (int index = 0; index < size; index++)
            if (elementData[index] == null) {
                fastRemove(index);
                return true;
            }
    } else {
        for (int index = 0; index < size; index++)
            if (o.equals(elementData[index])) {
                fastRemove(index);
                return true;
            }
    }
    return false;
}
```

如果ArrayList中有该元素，通过该元素第一次出现的位置，调用`void fastRemove(int index)`删除第一次出现的这个元素，并返回true，如果没有找到，则返回fasle。本方法允许删除值为null的元素。

### 修改某个索引的数据

```java
public E set(int index, E element) {
    rangeCheck(index);
    E oldValue = elementData(index);
    elementData[index] = element;
    return oldValue;
}
```

直接将对应索引的元素设置为指定的值，返回旧的元素。

### removeAll方法

```java
public boolean removeAll(Collection<?> c) {
    Objects.requireNonNull(c);
    return batchRemove(c, false);
}
```

从此ArrayList中删除所有包含在指定集合中的所有元素。与之相法的方法是`boolean retainAll(Collection<?> c)`，后者是保留所有包含在指定集合中的当前ArrayList中的元素，删除其它元素。

### removeRange方法

```java
protected void removeRange(int fromIndex, int toIndex) {
    modCount++;
    int numMoved = size - toIndex;
    System.arraycopy(elementData, toIndex, elementData, fromIndex, numMoved);

    // clear to let GC do its work
    int newSize = size - (toIndex-fromIndex);
    for (int i = newSize; i < size; i++) {
        elementData[i] = null;
    }
    size = newSize;
}
```

删除ArrayList中，索引从`fromIndex`（包含）到`toIndex`（不包含）中间的所有元素。

### addAll方法

```java
public boolean addAll(int index, Collection<? extends E> c) {
    rangeCheckForAdd(index);

    Object[] a = c.toArray();
    int numNew = a.length;
    ensureCapacityInternal(size + numNew);  // Increments modCount

    int numMoved = size - index;
    if (numMoved > 0)
        System.arraycopy(elementData, index, elementData, index + numNew, numMoved);

    System.arraycopy(a, 0, elementData, index, numNew);
    size += numNew;
    return numNew != 0;
}
```
将给定的集合`c`按其循序插入到本ArrayList的第`index`的位置，本ArrayList远`index`之后的数据直接跟在c的尾部。

### toArray

将ArrayList转换为数组

```java
public Object[] toArray() {
    return Arrays.copyOf(elementData, size);
}
```

这个方法直接将ArrayList的存储数组前`size`个元素到新数组并返回，实现中利用了`Arrays.copyOf`方法。

### toArray(T[] a)方法

```java
public <T> T[] toArray(T[] a) {
    if (a.length < size)
        // 创建一个新的运行时类型数组，但内容内容是当前ArrayList的:
        return (T[]) Arrays.copyOf(elementData, size, a.getClass());
    System.arraycopy(elementData, 0, a, 0, size);
    if (a.length > size)
        a[size] = null;
    return a;
}
```

如果传入数组`a`的长度小于`size`，返回一个新的数组，大小为size，类型与传入数组`a`相同。所传入数组长度与size相等，则将elementData复制到传入数组中并返回传入的数组。若传入数组长度大于size，除了复制elementData外，还将把返回数组的第size之后的元素置为空。
 如果数组`a`的运行时类型不是此ArrayList中每个元素的运行时类型的超类，则抛出`ArrayStoreException`异常。

### 浅拷贝方法

```java
    public Object clone() {
        try {
            ArrayList<?> v = (ArrayList<?>) super.clone();
            v.elementData = Arrays.copyOf(elementData, size);
            v.modCount = 0;
            return v;
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError(e);
        }
    }
```

返回当前ArrayList实例的浅拷贝，元素本身没有被复制。

其它

remvoeIf方法，删除所有满足指定判断的当前ArrayList中的元素。

sort方法，传入`comparator`，将当前ArrayList按给定的比较器排序。

subList方法，回指定索引（fromIndex和toIndex， 前闭后开）之间的部分视图。如果fromIndex和toIndex相等，返回长度为0的list。返回的List是原来List中的一部分的视图的引用。任何对返回的subList的修改都将同步到原List中，反之亦然。

```java
public List<E> subList(int fromIndex, int toIndex) {
    subListRangeCheck(fromIndex, toIndex, size);
    return new SubList(this, 0, fromIndex, toIndex);
}
```

## LinkedList中一些方法总结

### 插入节点到头部或尾部：

```java
private void linkFirst(E e) {
    final Node<E> f = first;
    final Node<E> newNode = new Node<>(null, e, f);
    first = newNode;
    if (f == null)
        last = newNode;
    else
        f.prev = newNode;
    size++;
    modCount++;
}
```

### 尾部插入数据接口与之类似，不再赘述。

删除一个节点：

```java
E unlink(Node<E> x) {
    // assert x != null;
    final E element = x.item;
    final Node<E> next = x.next;
    final Node<E> prev = x.prev;

    if (prev == null) {
        first = next;
    } else {
        prev.next = next;
        x.prev = null;
    }

    if (next == null) {
        last = prev;
    } else {
        next.prev = prev;
        x.next = null;
    }

    x.item = null;
    size--;
    modCount++;
    return element;
}
```

### 在给定节点succ前插入数据e

```java
void linkBefore(E e, Node<E> succ) {
    // assert succ != null;
    final Node<E> pred = succ.prev;
    final Node<E> newNode = new Node<>(pred, e, succ);
    succ.prev = newNode;
    if (pred == null)
        first = newNode;
    else
        pred.next = newNode;
    size++;
    modCount++;
}
```

### 删除头部或尾部的节点：

```java
 private E unlinkFirst(Node<E> f) {
    // assert f == first && f != null;
    final E element = f.item;
    final Node<E> next = f.next;
    f.item = null;
    f.next = null; // help GC
    first = next;
    if (next == null)
        last = null;
    else
        next.prev = null;
    size--;
    modCount++;
    return element;
}
```

插入尾部的方法类似，这里不在赘述。上面的代码显示，LinkedList的add方法（内部使用了linkFirst）、remove方法（内部使用的是unlink）可以接受null。即：LinkedList允许null元素。

### List接口提供的方法

作为List，LinkedList提供了`size()`，`add()`，`get()`，`contains()`，`remove()`，`addAll()`，以及`indexOf()`等。需要注意的是，对于基于索引的所有操作，LinkedList提供的List方法除了`size()`，很多方法的复杂度都至少为`O(index)`，需要考虑性能的场景请注意。对于所有的Index，如果Index超出了size大小，都会抛出`IndexOutOfBounsException`。
此外LinkedList提供了`toArray()`方法。

### Deque接口提供的方法

作为双向队列，LinkedList提供了很多方便的操作：

`getFirst() / getLast()`方法

```java
public E getFirst() {
        final Node<E> f = first;
        if (f == null)
            throw new NoSuchElementException();
        return f.item;
    }
```

`removeFirst() / removeLast()` 方法

```java
public E removeFirst() {
        final Node<E> f = first;
        if (f == null)
            throw new NoSuchElementException();
        return unlinkFirst(f);
    }
```

前面两组方法，如果对于的节点不存在，都会抛出`NoSuchElementException`。

`addFirst() / addLast()` 方法等。

offer相关的方法，包括`offer(e), offerFirst(e), offerLast()`等，他们分别和前面提到的`add(), addFirst()，addLast()`作用相同。

```java
public boolean offerFirst(E e) {
        addFirst(e);
        return true;
    }
```

peek方法，包括`peekFirst()`，`peekLast()`，用来检索返回第一个和最后一个节点的值，但不删除该节点。

```java
public E peekFirst() {
        final Node<E> f = first;
        return (f == null) ? null : f.item;
     }
```

与peek类似的方法是poll方法，包括`pollFirst()`，`pollLast()`,他们勇于检索第一个或最后一个节点的值返回，并删除这个节点。

pull方法，往队列头部插入节点，内部调用`addFirst()`。

pop方法，从头部检索并返回节点的值，然后删除该节点。

### 迭代器方法

LinkedList支持`ListIterator`和`Spliterator`两种迭代器。

从Java8 开始提供的`Spliterator`迭代器，可以单独遍历元素（`tryAdvance（）`），也可以批量遍历（`forEachRemaining（）`），也可以将一些元素（使用`trySplit（）`）作为另一个`Spliterator`进行分割，以用于可能的并行操作。需要注意的是`Spliterator`并不是线程安全的。

## 内部类对比

ArrayList的内部类包括两个迭代器：`private class Itr implements Iterator<E>`和`private class ListItr extends Itr implements ListIterator<E>`，以及一个子类类型`private class SubList extends AbstractList<E> implements RandomAccess`，它们是AbstractList中对应内部类的优化版本，不在赘述。