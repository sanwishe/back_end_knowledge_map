# 后端知识图谱

面试过程可能问到的一些知识点

## java基础

### 数据结构

- 链表和数组
    ArrayList与LinkedList
- 映射
- 队列
- 树
  - 二叉树
  - 红黑树

### 算法

- 排序算法
- 查找算法
- 字符串比较算法

### jvm

- 理论基础
- jmm
- jvm调优

### 并发

- 线程安全
- 多线程
- 锁
  - synchronized 与 volatile
  - cas
  - java中的锁与同步类
  - 公平锁与非公平锁
  - 乐观锁与悲观锁
  - 可重入锁

### io

- io
- nio
- netty

### java8等新特性

- Functional Interface
- Lambda
- Stream API
- 方法引用
- 默认方法

### 设计模式

- 设计模式的几大原则
- 单例模式
- 工厂模式
- 建造者模式
- 组合模式
- 代理模式
- 观察者模式
- 适配器模式
- MVC
- AOP

### 持久化

- 数据库基本理论
- mysql
- nosql
  - hbase
- elasticsearch

### 中间件

- kafka
- logstash
- tomcat
- jetty
- 服务端缓存
  - ehcache
  - memchche
  - Redis
- 定时调度
      - quartz
- rpc
      - Thrfit
      - gRpc
      - dobbo
- spring-cloud相关

### 性能

- profiler
- oracle jdk工具：jmap，jvirtualVM，jConsole，mat，dump
- benchmark：jmeter
- APM
- jvm调优
- 重构

### 微服务

- 概念
- 分布式理论
  - 负载均衡
  - HA
  - 一致性
    - cap理论与base理论
    - 分布式锁
    - 分布式一致性算法：paxos，raft
- docker
- kubernetes
- paas
- Service Mesh

### 工具

- IDE：Eclipse/IDEA等
- CI：jenkins
- review：git、gerrit