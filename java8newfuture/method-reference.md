# 方法引用

## 概念

方法引用通过方法的名字来指向一个方法。方法引用使用一对冒号 ::来引用方法。
当一个Lambda表达式调用了一个已存在的方法时，适合使用方法引用。但是如果这个方法需要传入lambda之外的参数，则不适合使用方法引用。
有以下四种形式的方法引用:
| 类型 | 示例 |
|:---:|:------: |
| 引用静态方法 | ContainingClass::staticMethodName |
| 引用某个对象的实例方法 | containingObject::instanceMethodName |
| 引用某个类型的任意对象的实例方法 | ContainingType::methodName |
| 引用构造方法 | ClassName::new |


## 示例

- 构造器引用

它的语法是`Class::new`，或者更一般的`Class< T >::new`实例如下：

```java
final Car car = Car.create( Car::new );
final List< Car > cars = Arrays.asList( car );
```

- 静态方法引用

它的语法是`Class::static_method`，实例如下：

```java
cars.forEach( Car::collide );
```

- 特定类的任意对象的方法引用

它的语法是`Class::method`实例如下：

```java
cars.forEach( Car::repair );
```

- 特定对象的方法引用

它的语法是`instance::method`实例如下：

```java
final Car police = Car.create( Car::new );
cars.forEach( police::follow );
```