# 函数式接口

java 8 增加了`java.util.function`包，提供了函数式接口。函数式接口(Functional Interface)就是一个有且仅有一个抽象方法，但是可以有多个非抽象方法的**接口**。函数式接口可以被隐式转换为lambda表达式。
java 8 之前也有一些函数式接口，如`Runnable，Callable，java.util.Comparator，java.io.FileFilter`等。java8提供了`java.util.function`，并引入了`@FunctionalInterface`注解，juf包提供了几大类函数式接口：

| 序号 | 接口 | 描述 |
| --- |:----------|:------------------------:|
| 1 | Supplier< T > | 无参数，调用时返回类型为T的结果。|
| 2 | Consumer< T > |提供一个参数，但是不返回任何结果，单纯消费参数传入的数据。 |
| 3 | Predicate<T> | 接受一个输入参数，返回一个布尔值结果。 |
| 4 | Function<T,R> | 接受一个输入参数，返回一个结果。 |

- `Consumer< T >`

Consumer接口中有2个方法，有且只有一个声明为accept(T t)的方法，接收一个输入参数并且没有返回值。Consumer接口表示一个接受单个输入参数并且没有返回值的操作。不像其他函数式接口，Consumer接口期望执行带有副作用的操作。其定义如下：

```java
public interface Function<T, R> {

    R apply(T t);

    default <V> Function<V, R> compose(Function<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v) -> apply(before.apply(v));
    }


    default <V> Function<T, V> andThen(Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t) -> after.apply(apply(t));
    }


    static <T> Function<T, T> identity() {
        return t -> t;
    }
}
```

- `Predicate<T>`

Predicate用来判断输入的对象是否符合某个条件，在Predicate接口中，有以下5个方法，除了test()方法是抽象方法以外，其他方法都是默认方法（译者注：在Java 8中，接口可以包含带有实现代码的方法，这些方法称为default方法）。可以使用匿名内部类提供test()方法的实现，也可以使用lambda表达式实现test()。如下：

```java
@FunctionalInterface
public interface Predicate<T> {

    boolean test(T t);

    default Predicate<T> and(Predicate<? super T> other) {
        Objects.requireNonNull(other);
        return (t) -> test(t) && other.test(t);
    }

    default Predicate<T> negate() {
        return (t) -> !test(t);
    }

    default Predicate<T> or(Predicate<? super T> other) {
        Objects.requireNonNull(other);
        return (t) -> test(t) || other.test(t);
    }

    static <T> Predicate<T> isEqual(Object targetRef) {
        return (null == targetRef)
                ? Objects::isNull
                : object -> targetRef.equals(object);
    }
}
```

- `Function<T,R>`

Function接受一个输入参数T，返回一个结果R，提供一个apply方法，将指定的参数T转化为输出结果R。

```java
@FunctionalInterface
public interface Function<T, R> {

    R apply(T t);

    default <V> Function<V, R> compose(Function<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v) -> apply(before.apply(v));
    }

    default <V> Function<T, V> andThen(Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t) -> after.apply(apply(t));
    }

    static <T> Function<T, T> identity() {
        return t -> t;
    }
}
```

- `Supplier<T>`

Supplier提供一个无参数的get方法，以返回一个结果T，用来生产某种结果。

```java
@FunctionalInterface
public interface Supplier<T> {

    T get();
}
```