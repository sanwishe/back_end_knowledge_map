# lambda表达式

## 什么是lambda

lambda是一段可以用来作为方法参数传递的代码，其核心思想是将面向对象中的数据传递变为行为传递。lambda表达式也称为闭包，使用lambda可以使应用代码变得简洁紧凑。

java 8 lambda的基本语法：

```java
expression = (parameters) -> action;
```

lambda表达式有以下特性：

- 可选的参数类型声明
- 可选的参数外层的圆括号（只有一个参数时）
- 可选的action外层的花括号（只有一个语句时）
- 可选的return关键字

需要注意的是，lambda表达式内部对变量的使用有以下要求：
1. lambda表达式内部不可以修改引用的外部变量，即lambda表达式内部引用的外部变量必须隐式的为final变量
2. lambda表达式内部的局部变量，不可以和外部的变量同名，也不可以被后续代码修改，即lambda表达式内部的局部变量隐性的具有final语义。

示例：

```java
new Thread(() -> System.out.println("hello, lambda.")).start();
```

```java
List<Integer> list = Arrays.asList(32, 87, 99, 39, 65, 11);

list.sort(Integer::compareTo);
```