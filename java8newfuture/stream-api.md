# stream api

java 8 中的stream api提供了对集合数据操作的全新方法。

**Stream 使用一种类似用 SQL 语句从数据库查询数据的直观方式来提供一种对 Java 集合运算和表达的高阶抽象。**

stream将数据集合看作流，流在管道中传输，在各个中间操作（intermediate operation）进行处理，在最终操作（terminal operation）得到处理结果，并由最终操作负责关闭流。

如：

```java
List<Integer> list = Arrays.asList(32, 87, 99, 39, 65, 11, 101, 77, 32, 63, 50, 233, 81);

int sum = list.stream().distinct().filter(integer -> integer >= 20 && integer <= 90).mapToInt(Integer::intValue).sum();

System.out.println(sum);
```

stream （流）是一种源自于数据集合的数据队列，队列中的数据支持聚合等相关操作

 - 数据源的元素是特定类型的对象
 - 数据源可以是集合，数组，io或者生成器等
 - 常见的操作有：filter, map, reduce, find, match, sorted等
 
和集合操作不同，stream的中间操作只会返回流对象本身，多个操作就可以串联成一个管道；集合对元素的迭代是在集合外部进行，而stream操作对集合的处理是在集合内部进行迭代的。

## 流的创建

- 从集合创建

```java
List<Integer> list = Arrays.asList(1, 2, 4);
Stream<Integer> stream = list.stream();
```

- 从数组创建

```java
int[] array = {32, 87, 99, 39, 65, 11, 101, 77, 32, 63, 50, 233, 81};

int sum = Arrays.stream(array).distinct().filter(i -> i > 20 && i <=100).sum();
```

- 从io创建

```java
try {
    Stream<String> stream = Files.lines(Paths.get("/Users/jmz/IdeaProjects/practise/resource/file"));

    stream.distinct().filter(str -> str.startsWith("j")).findFirst().ifPresent(System.out::println);
} catch (IOException e) {
    e.printStackTrace();
}
```

- 通过静态工厂方法创建流

```java
Stream<Integer> stream = Stream.of(32, 87, 99, 39, 65, 11, 101, 77, 32, 63, 50, 233, 81);

int sum = stream.distinct().filter(integer -> integer >= 20 && integer <= 90).mapToInt(Integer::intValue).sum();
```

## 中间操作

### `filter`

用于通过设置的条件过滤出元素

```java
Stream<T> filter(Predicate<? super T> predicate);
```

### `map`

用于将元素映射到对应的结果，输出的流中，元素已由mapper改变，相似的还有mapToInt等

```java
<R> Stream<R> map(Function<? super T, ? extends R> mapper);
```

如：

```java
Stream<Integer> stream = Stream.of(32, 87, 99, 39, 65, 11, 101, 77, 32, 63, 50, 233, 81);

stream.map(integer -> integer /2.0).forEach(System.out::println);
```

### `distinct`

用于去重

```java
Stream<T> distinct();
```

### `sorted`

用于排序，如果没有参数，按照自然顺序排序，否则按照传入的comparator 排序。

### `peek`

生成一个包含原Stream的所有元素的新Stream，同时会提供一个消费函数（Consumer实例），新Stream每个元素被消费的时候都会执行给定的消费函数

如：

```java
try {
    Stream<String> stream = Files.lines(Paths.get("/Users/jmz/IdeaProjects/practise/resource/file"));

    stream.peek(System.out::println).distinct().filter(str -> str.startsWith("j")).findFirst().ifPresent(System.out::println);

    } catch (IOException e) {
        e.printStackTrace();
    }
}
```

### `limit/skip`

limit用来截断前n个元素，skip用来跳过前m个元素

如

```java
Stream<Integer> stream = Stream.of(32, 87, 99, 39, 65, 11, 101, 77, 32, 63, 50, 233, 81);
stream.skip(4).limit(7).forEach(System.out::println);
```
打印从65开始后的七个元素，65到50。

## 终结操作

### `foreach`

将流中的元素按照提供的`Consumer`方法消费，返回空，而且会关闭流。

### `collect`

收集结果，如：

```java
Stream<Integer> stream = Stream.of(32, 87, 99, 39, 65, 11, 101, 77, 32, 63, 50, 233, 81);
List<Integer> newList = stream.map(integer -> integer * 2).collect(Collectors.toList());
```

### `reduce`

`reduce`方法提供了一个起始值（种子），然后依照运算规则（`BinaryOperator`），和前面 Stream 的第一个、第二个、第 n 个元素组合运算，得到结果。
如

```java
Stream<Integer> stream = Stream.of(32, 87, 99, 39, 65, 11, 101, 77, 32, 63, 50, 233, 81);
int max = stream.reduce(Integer.MAX_VALUE, Math::min);
```

reduce方法也可以只提供一个运算规则，返回optional对象，如：

```java
Stream<Integer> stream = Stream.of(32, 87, 99, 39, 65, 11, 101, 77, 32, 63, 50, 233, 81);
int sum = stream.reduce(Integer::sum).orElse(0);
```

### `anyMatch/allMatch/noneMatch`

用来检测任意/所有元素是否满足给定条件，肯定则返回true，否则为false。

### `findFirst/findAny`

检测流操作结果的第一个元素，返回optional对象。